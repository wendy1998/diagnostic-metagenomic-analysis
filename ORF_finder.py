#!/usr/bin/env python3
"""
vindt ORFs in een gegeven fasta file
    usage: /.ORF_finder.py -f fasta file
"""

# IMPORTS
import sys
import re
import argparse

# METADATA VARIABLES
__author__ = "Wendy van der Meulen"
__status__ = "Python Script"
__version__ = "0.1"

# GLOBAL VARIABLES


# CLASSES
class SearchEngine:
    """Searches orfs"""
    def __init__(self, seq):
        self.seq = self.set_sequence(seq)

        # work
        self.startpoints = self.find_startpoints()
        self.stoppoints = self.find_stoppoints()
        self.orfs = self.find_orfs()

    # setters
    def set_sequence(self, seq):
        """Sets the value for the sequence objects"""
        return seq.upper().strip()

    def find_startpoints(self):
        """Find all the startpoints in the sequence"""
        pattern = "ATG"
        positions = []
        for match in re.finditer(pattern, self.seq):
            positions.append(match.start())

        return positions

    def find_stoppoints(self):
        """Find all the stoppoints in the sequence"""
        pattern = "(T[AG]{2})"
        positions = []
        for match in re.finditer(pattern, self.seq):
            if match.groups()[0] != "TGG":
                positions.append(match.start())

        return positions

    def find_orfs(self):
        """Finds the actual orfs in the sequence"""
        orfs = []
        for start in self.startpoints:
            for stop in self.stoppoints:
                if stop > start:
                    if (stop - start)%3 == 0:
                        orfs.append((start, stop, self.seq[start:stop+3]))
                        break

        return orfs

    # getters
    def get_orfs(self):
        """returns the orfs"""
        return self.orfs

    def __str__(self):
        out = ["sequence:{0}{1}ORFs:{1}"]
        for orf in self.orfs:
            out.append("ORF: " + orf[2] + " Start: " + orf[0] + " Stop: " + orf[1] + "{1}")


# DEFINITIONS
def parse_commandline():
    """Parses commandline arguments"""
    parser = argparse.ArgumentParser()

    # add possible arguments for parser
    parser.add_argument("-f", "--fasta", help="Put the Fasta file in here.")
    parser.add_argument("-out", "--output", help="Place the name you wish to give to the output file in here.")

    # check if enough arguments were given
    if len(sys.argv) != 5:
        parser.print_help()
        sys.exit()

    # make shortcut
    args = parser.parse_args()

    fasta_file = args.fasta
    output_file = args.output

    return fasta_file, output_file


def process_fasta(file):
    """
returns the different sequences in the fasta file with their header.
 """
    try:
        with open(file) as open_file:
            lines = []
            # adds all lines in the fasta file to a list.
            for line in open_file:
                lines.append(line)

        # >header#seq>header#seq...
        information = "#".join(lines).split('>')  # after split: [>, header#seq, header#seq, ...]
        # removes single > at the start of the list
        information.remove(information[0])
        seqs = {}
        for info in information:
            seq = "".join(info.split('#')[1:]).strip()
            header = info.split('#')[0].strip()
            seqs[header] = seq
        return seqs
    except IOError as e:
        print("{} ({})".format(type(e).__name__, e.args[0]))
        print("exiting now...")
        sys.exit()


def process_orfs(seqs, output_file):
    """Processes the found orfs"""
    with open(output_file, "w") as open_file:
        orfs_finder = SearchEngine
        for name, seq in zip(seqs.keys(), seqs.values()):
            orfs = orfs_finder(seq).get_orfs()
            for counter, orf in enumerate(orfs):
                out = ">contig " + name + ", orf" + str(counter + 1) + \
                      ", Start: " + str(orf[0]) + ", Stop: " + str(orf[1] + 2) + "\n" + orf[2] + "\n"
                open_file.write(out)

    return 0


# MAIN
def main():
    """Calls all the funtions"""
    # PREPARATIONS
    args = parse_commandline()

    # WORK
    seqs = process_fasta(args[0])
    process_orfs(seqs, args[1])

    # FINISH
    return 0

# RUN
if __name__ == "__main__":
    sys.exit(main())
