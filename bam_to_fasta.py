#!/usr/bin/env python3
"""
Program that converts a bam file to a fasta file
Usage: ./bam_to_fasta.py in_file out_file
"""

# IMPORTS
import sys
import pysam
from Bio import SeqIO, Seq, SeqRecord


# DEFINITIONS
def bam_to_rec(in_file):
    """
    Generator to convert BAM files into Biopython SeqRecords.
    """
    bam_file = pysam.Samfile(in_file, "rb")
    for read in bam_file:
        seq = Seq.Seq(read.seq)
        if read.is_reverse:
            seq = seq.reverse_complement()
        rec = SeqRecord.SeqRecord(seq, read.qname, "", "")
        yield rec


# MAIN
def main(in_file, out_file):
    with open(out_file, "w") as out_handle:
        # Write records from the BAM file one at a time to the output file.
        # Works lazily as BAM sequences are read so will handle large files.
        SeqIO.write(bam_to_rec(in_file), out_handle, "fasta")

# RUN
if __name__ == "__main__":
    main(*sys.argv[1:])
