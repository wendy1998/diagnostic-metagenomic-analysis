import sys


def process_fasta(file):
    """
returns the different sequences in the fasta file with their header.
 """
    try:
        with open(file) as open_file:
            lines = []
            # adds all lines in the fasta file to a list.
            for line in open_file:
                lines.append(line)

        # >header#seq>header#seq...
        information = "#".join(lines).split('>')  # after split: [>, header#seq, header#seq, ...]
        # removes single > at the start of the list
        information.remove(information[0])
        seqs = {}
        for counter, info in enumerate(information):
            seq = "".join(info.split('#')[1:]).strip()
            header = info.split('#')[0].strip()
            seqs[header] = seq
            if counter == 99:
                break
        return seqs
    except IOError as e:
        print("{} ({})".format(type(e).__name__, e.args[0]))
        print("exiting now...")
        sys.exit()


def write_to_file(seqs, out):
    with open(out, "w") as open_file:
        for name, seq in zip(seqs.keys(), seqs.values()):
            open_file.write(">" + name + "\n")
            open_file.write(seq)
            open_file.write("\n")

fasta = input("Which fasta file do you want to process? ")
out = input("What do you want to name the output file? ")
seqs = process_fasta(fasta)
write_to_file(seqs, out)
